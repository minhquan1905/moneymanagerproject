## Node js + MongoDB
- Xác thực request bằng JSON web token.

## Model Descriptions
- User biểu diễn một người dùng của ứng dụng:
    + ID: String
    + name: String
    + email: String
    + password: String
    + job: String
    + address: String
    + birthdate: String

- Entry biểu diễn một chi phí hoặc thu nhập:
    + ID: String
    + price: String
    + name: String
    + type: String
    + note: String
    + date: String
    + userId: String => ref tới User model

## APIS
- User
    + Register: [POST] https://co3043hk182.herokuapp.com/auth/register
    + Login: [POST] https://co3043hk182.herokuapp.com/auth/sign_in
        * Token return: { "token": "..."} => Lưu xuống sqllite, dùng token mỗi khi request đến server
    + Update profile: [PUT] https://co3043hk182.herokuapp.com//update-profile/5caea513b3503400041d23cd
        * Set request header: key: "authorization"; value: "JWT token"
    + Reset password: [POST] https://co3043hk182.herokuapp.com/reset-password

- Entry:
    + Create entry: [POST] https://co3043hk182.herokuapp.com/entry
        * Set request header: key: "authorization"; value: "JWT token"
    + Delete entry: [DELETE] https://co3043hk182.herokuapp.com/entry/5caea910b3503400041d23d0
        * Set request header: key: "authorization"; value: "JWT token"
    + Update entry: [PUT] https://co3043hk182.herokuapp.com/entry/5caea910b3503400041d23d0
        * Set request header: key: "authorization"; value: "JWT token"
    + Get one entry: [GET] https://co3043hk182.herokuapp.com/entry/5caea910b3503400041d23d0
    + Get all entry by userid: [GET] https://co3043hk182.herokuapp.com/entries?userid=5caea513b3503400041d23cd 
        * Set request header: key: "authorization"; value: "JWT token"
        * Return JSON array
