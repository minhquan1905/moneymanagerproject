'use strict';

var express = require('express'),
	app = express(),
	port = process.env.PORT || 3000,
	mongoose = require('mongoose'),
	Entry = require('./api/models/entryModel'),
	User = require('./api/models/userModel'),
	bodyParser = require('body-parser'),
	jsonwebtoken = require("jsonwebtoken");

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 50000 }));

let mongoURI = '';
if (process.env.NODE_ENV === 'production') {
	mongoURI = 'mongodb://binhdo2:abc123@ds137596.mlab.com:37596/money-manager'
} else {
	mongoURI = 'mongodb://localhost/moneymanager'
}

mongoose.Promise = global.Promise;
mongoose.connect(mongoURI, {useNewUrlParser: true});


app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(bodyParser.json());

app.use(function (req, res, next) {
	if (req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === 'JWT') {
		jsonwebtoken.verify(req.headers.authorization.split(' ')[1], 'RESTFULAPIs', function (err, decode) {
			if (err) req.user = undefined;
			req.user = decode;
			console.log('>>>>>> req.user', decode)
			next();
		});
	} else {
		req.user = undefined;
		next();
	}
});
var routes = require('./api/routes/routes');
routes(app);

app.use(function (req, res) {
	res.status(404).send({
		url: req.originalUrl + ' not found'
	})
});

app.listen(port);

console.log('Server started on: ' + port);

module.exports = app;