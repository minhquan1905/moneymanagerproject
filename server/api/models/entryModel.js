var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var EntrySchema = new Schema({
	date: String,
	price: String,
    type: String,
    name: String,
    icon: String,
    note: String,
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
});


module.exports = mongoose.model('Entry', EntrySchema);