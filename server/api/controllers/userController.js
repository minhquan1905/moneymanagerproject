
var mongoose = require('mongoose'),
	jwt = require('jsonwebtoken'),
	bcrypt = require('bcrypt'),
	User = mongoose.model('User'),
	nodemailer = require('nodemailer');

var package = require('../../package');

exports.register = function (req, res) {
	var newUser = new User(req.body);
	newUser.hash_password = bcrypt.hashSync(req.body.password, 10);
	newUser.save(function (err, user) {
		if (err) {
			return res.status(400).send({
				message: err
			});
		} else {
			user.hash_password = undefined;
			return res.json(user);
		}
	});
};

exports.sign_in = function (req, res) {
	User.findOne({
		email: req.body.email
	}, function (err, user) {
		if (err) throw err;
		if (!user || !user.comparePassword(req.body.password)) {
			return res.status(401).json({
				message: 'Authentication failed. Invalid user or password.'
			});
		}
		return res.json({
			_id: user._id,
			fullName: user.fullName,
			email: user.email,
			birthDate: user.birthDate,
			job: user.job,
			phone: user.phone,
			avatar: user.avatar,
			address: user.address,
			gender: user.gender,
			token: jwt.sign({
				email: user.email,
				fullName: user.fullName,
				_id: user._id
			}, 'RESTFULAPIs')
		});
	});
};

exports.loginRequired = function (req, res, next) {
	if (req.user) {
		next();
	} else {
		return res.status(401).json({
			message: 'Unauthorized user!'
		});
	}
};

exports.updateProfile = function (req, res, next) {

	if (req.body.password) {
		req.body.hash_password = bcrypt.hashSync(req.body.password, 10);
	}

	User.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true }, (error, item) => {
		if (error) {
			res.status(500).send(error);
		} else if (item) {
			res.json(item);
		} else {
			res.status(400).send({ error: 'Error' });
		}
	});
}

function randomStringGenerator(stringLength) {
	var randomString = ""; // Empty value of the selective variable
	const allCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"; // listing of all alpha-numeric letters
	while (stringLength--) {
		randomString += allCharacters.substr(Math.floor((Math.random() * allCharacters.length) + 1), 1); // selecting any value from allCharacters varible by using Math.random()
	}
	return randomString; // returns the generated alpha-numeric string
}

const sendEmail = (mailTo, mailCc, mailSubject, mailText, mailHtml, mailAttachments, successCallback, errorCallback) => {
	var transporter = nodemailer.createTransport({
		service: 'gmail',
		auth: {
			user: package.email.from,
			pass: package.email.password
		},
		debug: true
	});

	transporter.on('log', console.log);

	var mailOptions = {
		from: package.email.from,
		cc: mailCc.toString(),
		to: mailTo,
		subject: mailSubject,
		text: mailText,
		html: mailHtml,
		attachments: mailAttachments
	};

	transporter.sendMail(mailOptions, (error, info) => {
		if (error) {
			console.log(error);
			if (errorCallback) errorCallback(error);
		} else {
			console.log('Send mail to ' + mailTo + ' successful.');
			if (successCallback) successCallback();
		}
	});
};

exports.resetPassword = function (req, res, next) {
	User.findOne({ email: req.body.email }, (error, item) => {
		if (error) {
			res.status(500).send(error);
		} else if (item) {
			var newPassword = randomStringGenerator(6);
			item.hash_password = bcrypt.hashSync(newPassword, 10);
			item.save((error, user) => {
				sendEmail(user.email, [], 'New password', '', 'Your new password is ' + newPassword, [], (error) => {
					if (error) {
						res.json({
							error: 'Error while sending email'
						})
					} else {
						res.json(item);
					}
				});
			})
		} else {
			res.status(400).send({ error: 'Email not found' });
		}
	});
}
