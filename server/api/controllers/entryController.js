var mongoose = require('mongoose');
var Entry = mongoose.model('Entry');

exports.getEntry = (req, res) => {
    Entry.findById(req.params.id).exec((error, item) => {
        if (error) {
            res.status(500).send({error});
        } else if (item) {
            res.json(item);
        } else {
            res.status(400).send({error: 'Error'})
        }
    })
}

exports.getAllEntries = (req, res) => {
    Entry.find({userId: req.query.userid}).exec((error, items) => {
        if (error) {
            res.status(500).send({error});
        } else if (items) {
            res.json(items);
        } else {
            res.status(400).send({error: 'Error'})
        }
    })
}

exports.createEntry = (req, res) => {
    Entry.create(req.body, (error, item) => {
        if (error) {
            res.status(500).send({error});
        } else if (item) {
            res.json(item);
        } else {
            res.status(400).send({error: 'Error'})
        }
    });
}

exports.updateEntry = (req, res) => {
    Entry.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}, (error, item) => {
        if (error) {
            res.status(500).send(error);
        } else if (item) {
            res.json(item)
        } else {
            res.status(400).send({error: 'Error'})
        }
    });
}

exports.deleteEntry = (req, res) => {
    Entry.remove({_id: req.params.id}, (error) => {
        if (error) {
            res.status(400).send({error});
        } else {
            res.json({message: 'Entry deleted!'})
        }
    })
}