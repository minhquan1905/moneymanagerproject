'use strict';

module.exports = function(app) {
	var entryHandlers = require('../controllers/entryController.js'),
	userHandlers = require('../controllers/userController.js');

	app.route('/auth/register')
		.post(userHandlers.register);

	app.route('/auth/sign_in')
		.post(userHandlers.sign_in);

	app.route('/update-profile/:id')
		.put(userHandlers.loginRequired, userHandlers.updateProfile)

	app.route('/entry')
		.post(userHandlers.loginRequired, entryHandlers.createEntry);

	app.route('/entries')
		.get(userHandlers.loginRequired, entryHandlers.getAllEntries);

	app.route('/entry/:id')
		.get(entryHandlers.getEntry)
		.put(userHandlers.loginRequired, entryHandlers.updateEntry)
		.delete(userHandlers.loginRequired, entryHandlers.deleteEntry);

	app.route('/reset-password')
		.post(userHandlers.resetPassword);
};
