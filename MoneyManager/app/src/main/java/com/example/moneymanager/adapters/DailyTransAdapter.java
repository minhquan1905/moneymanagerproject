package com.example.moneymanager.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.moneymanager.R;
import com.example.moneymanager.models.DailyTransactionModel;

import java.util.ArrayList;
import java.util.List;

public class DailyTransAdapter extends RecyclerView.Adapter<DailyTransAdapter.DailyTransViewHolder> {
    private List<DailyTransactionModel> list = new ArrayList<>();
    private Context context;

    public DailyTransAdapter(Context context){
        this.context = context;
    }

    public void setTransaction(List<DailyTransactionModel> list){
        this.list.addAll(list);
    }

    @NonNull
    @Override
    public DailyTransViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_home_in_day,viewGroup,false);

        return new DailyTransViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DailyTransViewHolder dailyTransViewHolder, int i) {
        DailyTransactionModel model = list.get(i);
        dailyTransViewHolder.totalTrans.setText(model.getTotalTrans());
        dailyTransViewHolder.fullDate.setText(model.getFullDay());
        HomeAdapter homeAdapter = new HomeAdapter(this.context);
        homeAdapter.setTransaction(model.getList());
        dailyTransViewHolder.recyclerView.setLayoutManager(new LinearLayoutManager(this.context));
        dailyTransViewHolder.recyclerView.setAdapter(homeAdapter);


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class DailyTransViewHolder extends RecyclerView.ViewHolder {
        public TextView date,fullDate,totalTrans;
        public RecyclerView recyclerView;

        public DailyTransViewHolder(@NonNull View itemView) {
            super(itemView);
            fullDate = itemView.findViewById(R.id.fullDay);
            totalTrans = itemView.findViewById(R.id.totalTrans);
            recyclerView = itemView.findViewById(R.id.recyclerViewTrans);
        }


    }
}

