package com.example.moneymanager.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.moneymanager.activity.ChagePasswordActivity;
import com.example.moneymanager.activity.ChangeLanguageActivity;
import com.example.moneymanager.activity.LoginActivity;
import com.example.moneymanager.models.Function;
import com.example.moneymanager.adapters.FunctionAdapter;
import com.example.moneymanager.R;
import com.example.moneymanager.storage.SharedPrefManager;

import java.util.ArrayList;
import java.util.List;


public class More extends Fragment {

    private List<Function> functions;
    private ListView lsvFunction;

    private final String SETTING = "setting";
    private final String CHANGE_PASSWORD = "change_password";
    private final String EXPORT = "export";
    private final String LOGOUT = "logout";

    public More() {

    }

    public static More newInstance() {
        More fragment = new More();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_more, container, false);

        lsvFunction = view.findViewById(R.id.lsvFunction);

        functions = getListData();
        lsvFunction.setAdapter(new FunctionAdapter(getActivity(), functions));

        lsvFunction.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String functionID = functions.get(position).getId();
                switch (functionID){
                    case SETTING:
                        startActivity(new Intent(getActivity(), ChangeLanguageActivity.class));
                        break;
                    case CHANGE_PASSWORD:
                        startActivity(new Intent(getActivity(), ChagePasswordActivity.class));
                        break;
                    case EXPORT:
                        Toast.makeText(getContext(), "Export", Toast.LENGTH_SHORT).show();
                        break;
                    case LOGOUT:
                        SharedPrefManager.getInstance(getActivity()).clear();
                        startActivity(new Intent(getActivity(), LoginActivity.class));
                        break;
                }
            }
        });

        return view;
    }

    private  List<Function> getListData() {
        List<Function> list = new ArrayList<>();

        list.add(new Function(SETTING,getResources().getString(R.string.list_item_setting), "settings_icon"));
        list.add(new Function(CHANGE_PASSWORD,getResources().getString(R.string.list_item_change_password), "password_icon"));
        list.add(new Function(EXPORT,getResources().getString(R.string.list_item_export), "export_to_file"));
        list.add(new Function(LOGOUT,getResources().getString(R.string.list_item_logout), "session_logout"));

        return list;
    }


}
