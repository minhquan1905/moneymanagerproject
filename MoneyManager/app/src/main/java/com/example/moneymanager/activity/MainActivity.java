package com.example.moneymanager.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.widget.TextView;

import com.example.moneymanager.R;
import com.example.moneymanager.fragment.Account;
import com.example.moneymanager.fragment.Report;
import com.example.moneymanager.fragment.More;
import com.example.moneymanager.fragment.TransactionsFragment;
import com.example.moneymanager.storage.SharedPrefManager;
import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;
import com.luseen.spacenavigation.SpaceOnClickListener;

public class MainActivity extends BaseActivity {

    ActionBar toolbar;
    TextView toolBarTittle;
    SpaceNavigationView spaceNavigationView ;
    Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (SharedPrefManager.getInstance(MainActivity.this).isFirstStart()){
            startActivity(new Intent(this, IntroActivity.class));
        }else if (!SharedPrefManager.getInstance(MainActivity.this).isLoggedIn()){
            startActivity(new Intent(this, LoginActivity.class));
        }

        toolBarTittle = findViewById(R.id.toolBarTittle);
        toolbar = getSupportActionBar();
        toolBarTittle.setText(getResources().getString(R.string.bottom_nav_dashboard));

        fragment = TransactionsFragment.newInstance();
        switchFragment(fragment);

        spaceNavigationView  = findViewById(R.id.bottomNav);
        spaceNavigationView.initWithSaveInstanceState(savedInstanceState);
        spaceNavigationView.addSpaceItem(new SpaceItem("HOME",R.drawable.ic_home_black_24dp));
        spaceNavigationView.addSpaceItem(new SpaceItem("REPORT",R.drawable.ic_pie_chart_black_24dp));
        spaceNavigationView.addSpaceItem(new SpaceItem("ACCOUNT",R.drawable.ic_account_circle_black_24dp));
        spaceNavigationView.addSpaceItem(new SpaceItem("MORE",R.drawable.ic_more_horiz_black_24dp));
        spaceNavigationView.showIconOnly();
        spaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {

            @Override
            public void onCentreButtonClick() {
                Intent intent = new Intent(MainActivity.this, AddTransactionActivity.class);
                startActivity(intent);
            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {
                switch (itemIndex){
                    case 0:
                        toolBarTittle.setText(getResources().getString(R.string.bottom_nav_dashboard));
                        fragment = TransactionsFragment.newInstance();
                        switchFragment(fragment);
                        break;
                    case 1:
                        toolBarTittle.setText(getResources().getString(R.string.bottom_nav_report));
                        fragment = Report.newInstance();
                        switchFragment(fragment);
                        break;
                    case 2:
                        toolBarTittle.setText(getResources().getString(R.string.bottom_nav_account));
                        fragment = Account.newInstance();
                        switchFragment(fragment);
                        break;
                    case 3:
                        toolBarTittle.setText(getResources().getString(R.string.bottom_nav_more));
                        fragment = More.newInstance();
                        switchFragment(fragment);
                        break;
                }

            }

            @Override
            public void onItemReselected(int itemIndex, String itemName) {

            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        spaceNavigationView.onSaveInstanceState(outState);
    }

    private void switchFragment(Fragment fragment){
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container,fragment)
                .addToBackStack(fragment.getTag())
                .commit();
    }

    public static void reStart(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}
