package com.example.moneymanager.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.moneymanager.R;
import com.example.moneymanager.interfaces.GetDataService;
import com.example.moneymanager.models.User;
import com.example.moneymanager.storage.SharedPrefManager;
import com.example.moneymanager.utils.RetrofitClientInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.moneymanager.utils.CheckValid.checkEditText;
import static com.example.moneymanager.utils.CheckValid.checkLengthPassword;
import static com.example.moneymanager.utils.CheckValid.checkPassword;

public class LoginActivity extends BaseActivity {

    private EditText edtEmail, edtPassword;
    private TextView txtForgotPassword, txtRegister;
    private Button btnLogin;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        addControls();
        addEvents();
    }

    private void addEvents() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = edtEmail.getText().toString().trim();
                String password = edtPassword.getText().toString().trim();
                loginAccount(username, password);
            }
        });

        txtRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });

        txtForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
            }
        });
    }

    private void loginAccount(String username, String password) {
        if (checkEditText(edtEmail) && checkLengthPassword(edtPassword)) {
            pDialog.show();

            final GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
            Call<User> call = service.signIn(username, password);

            call.enqueue(new Callback<User>() {

                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    pDialog.dismiss();

                    if (response.isSuccessful()){
                        SharedPrefManager.getInstance(LoginActivity.this).saveUser(response.body());
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    } else {
                        new AlertDialog.Builder(LoginActivity.this)
                                .setTitle("Hừm...")
                                .setMessage(getResources().getString(R.string.error_login))
                                .setPositiveButton(android.R.string.ok, null)
                                .show();
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    pDialog.dismiss();
                    new AlertDialog.Builder(LoginActivity.this)
                            .setTitle("Hừm...")
                            .setMessage(getResources().getString(R.string.error_connec))
                            .setPositiveButton(android.R.string.ok, null)
                            .show();
                }
            });
        }
    }

    private void addControls() {
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);
        txtForgotPassword = findViewById(R.id.txtForgot_password);
        txtRegister = findViewById(R.id.txtRegister);
        btnLogin = findViewById(R.id.btnLogin);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage(getResources().getString(R.string.pd_connecting));
        pDialog.setCanceledOnTouchOutside(false);
    }
}
