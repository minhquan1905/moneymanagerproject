package com.example.moneymanager.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.moneymanager.R;

public class TypeTransactionActivity extends AppCompatActivity {
    TextView tvCancel, tvChiTieu, tvThuNhap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_type_transaction);

        tvCancel = findViewById(R.id.tvCancel);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleChange(0);
            }
        });
        tvChiTieu = findViewById(R.id.tvChiTieu);
        tvThuNhap = findViewById(R.id.tvThuNhap);
        tvChiTieu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleChange(1);
            }
        });
        tvThuNhap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleChange(2);
            }
        });

    }

    private void handleChange(int key){
        Intent intent;
        Bundle b1;
        b1 = getIntent().getExtras();
        if (b1 != null) {
            if (b1.getString("class").equals("detail")) {
                intent = new Intent(TypeTransactionActivity.this, DetailTransactionActivity.class);
            } else {
                intent = new Intent(TypeTransactionActivity.this, AddTransactionActivity.class);
            }
        } else {
            intent = new Intent(TypeTransactionActivity.this, AddTransactionActivity.class);
        }
        Bundle b2 = new Bundle();
        b2.putInt("key", key);
        intent.putExtras(b2);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }
}
