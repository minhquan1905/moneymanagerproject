package com.example.moneymanager.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.moneymanager.R;
import com.example.moneymanager.adapters.GroupAdapter;
import com.example.moneymanager.models.GroupModel;
import java.util.ArrayList;
import java.util.List;

public class GroupActivity extends AppCompatActivity {
    TextView tvCancel, tvName;
    RecyclerView recyclerView;
    ImageView imgIcon;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);

        mapId();

        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        List<GroupModel> groups = new ArrayList<>();
        groups.add(new GroupModel(getResources().getDrawable(R.drawable.group_giai_tri), "Giải trí"));
        groups.add(new GroupModel(getResources().getDrawable(R.drawable.group_luong_bong), "Lương bổng"));
        groups.add(new GroupModel(getResources().getDrawable(R.drawable.group_nuoc), "Nước"));
        groups.add(new GroupModel(getResources().getDrawable(R.drawable.group_quan_ao), "Thời trang"));
        groups.add(new GroupModel(getResources().getDrawable(R.drawable.group_quyen_gop), "Quyên góp"));
        groups.add(new GroupModel(getResources().getDrawable(R.drawable.group_sach), "Sách"));
        groups.add(new GroupModel(getResources().getDrawable(R.drawable.group_shopping), "Shopping"));
        groups.add(new GroupModel(getResources().getDrawable(R.drawable.group_suc_khoe), "Sức khỏe"));
        groups.add(new GroupModel(getResources().getDrawable(R.drawable.group_taxi), "Taxi"));
        groups.add(new GroupModel(getResources().getDrawable(R.drawable.group_thiet_bi), "Thiết bị"));
        groups.add(new GroupModel(getResources().getDrawable(R.drawable.group_thuc_an), "Thức ăn"));
        groups.add(new GroupModel(getResources().getDrawable(R.drawable.group_thue_nha), "Thuê nhà"));
        groups.add(new GroupModel(getResources().getDrawable(R.drawable.group_y_te), "Y tế"));

        GroupAdapter groupAdapter = new GroupAdapter(this,groups);
        recyclerView.setAdapter(groupAdapter);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleChange();
            }
        });

    }

    private void mapId(){
        recyclerView = findViewById(R.id.recGroup);
        tvCancel = findViewById(R.id.tvCancel);
        tvName = findViewById(R.id.tvName);
        imgIcon = findViewById(R.id.imgIcon);
    }

    private void handleChange(){
        Intent intent;
        Bundle b1;
        b1 = getIntent().getExtras();
        if (b1 != null) {
            if (b1.getString("class").equals("detail")) {
                intent = new Intent(GroupActivity.this, DetailTransactionActivity.class);
            } else {
                intent = new Intent(GroupActivity.this, AddTransactionActivity.class);
            }
        } else {
            intent = new Intent(GroupActivity.this, AddTransactionActivity.class);
        }
        Bundle b2 = new Bundle();
        b2.putString("name", "");
        b2.putByteArray("icon", null);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }
}
