package com.example.moneymanager.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Base64;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.moneymanager.R;
import com.example.moneymanager.activity.LoginActivity;
import com.example.moneymanager.interfaces.GetDataService;
import com.example.moneymanager.models.User;
import com.example.moneymanager.storage.SharedPrefManager;
import com.example.moneymanager.utils.RetrofitClientInstance;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Account extends Fragment {

    private ImageView imgAvatar;
    private TextView txtFullName;
    private EditText edtFullName, edtPhone, edtBirthDay, edtAddress, edtJob;
    private RadioGroup rdgGender;
    private Button btnUpdate;
    private ProgressDialog pDialog;

    private final int PICK_IMAGE = 12345;
    private final int TAKE_PICTURE = 6352;
    private static final int REQUEST_CAMERA_ACCESS_PERMISSION =5674;
    private Bitmap bitmap = null;

    final Calendar myCalendar = Calendar.getInstance();

    public Account() {

    }

    public static Account newInstance() {
        Account fragment = new Account();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, container, false);
        addControls(view);
        addEvents();

        return view;
    }

    private void addEvents() {
        loadUserInformation();
        registerForContextMenu(imgAvatar);

        imgAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().openContextMenu(v);
            }
        });

        edtBirthDay.setKeyListener(null);
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };

        edtBirthDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                User userUpdate = SharedPrefManager.getInstance(getActivity()).getUser();

                String fullName = edtFullName.getText().toString().trim();
                String phone = edtPhone.getText().toString().trim();
                String gender = null;
                switch (rdgGender.getCheckedRadioButtonId()){
                    case R.id.rdbMale:
                        gender = "male";
                        break;
                    case R.id.rdbFemale:
                        gender = "female";
                        break;
                }
                String address = edtAddress.getText().toString().trim();
                String job = edtJob.getText().toString().trim();
                String birthDate = edtBirthDay.getText().toString().trim();


                userUpdate.setFullName(fullName);
                userUpdate.setPhone(phone);
                userUpdate.setGender(gender);
                userUpdate.setAddress(address);
                userUpdate.setJob(job);
                userUpdate.setBirthDate(birthDate);

                //Get avatar
                if (bitmap != null){
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream .toByteArray();
                    String encodedAvatar = Base64.encodeToString(byteArray, Base64.DEFAULT);
                    userUpdate.setAvatar(encodedAvatar);
                }

                updateProfile(userUpdate);
            }
        });
    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        edtBirthDay.setText(sdf.format(myCalendar.getTime()));
    }

    private void updateProfile(final User user) {
        pDialog.show();
        final GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<User> call = service.updateProfile("JWT " + user.getToken(), user.get_id() ,user.getFullName(),
                user.getPhone(), user.getGender(), user.getAddress(), user.getJob(), user.getBirthDate(), user.getAvatar());

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                pDialog.dismiss();
                if (response.isSuccessful()){
                    //update share
                    SharedPrefManager.getInstance(getActivity()).saveUser(user);
                    Toast.makeText(getActivity(), "Cập nhật thông tin thành công", Toast.LENGTH_SHORT).show();

                    //Reload fragment
                    Fragment currentFragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
                    if (currentFragment instanceof Account) {
                        FragmentTransaction fragTransaction = (getActivity()).getSupportFragmentManager().beginTransaction();
                        fragTransaction.detach(currentFragment);
                        fragTransaction.attach(currentFragment);
                        fragTransaction.commit();
                    }

                } else {
                    switch (response.code()) {
                        case 404:
                            Toast.makeText(getActivity(), "not found", Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Toast.makeText(getActivity(), "server broken", Toast.LENGTH_SHORT).show();
                            break;
                        default:
                            Toast.makeText(getActivity(), "unknown error", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(getActivity(), "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void loadUserInformation() {
        User userLogin = SharedPrefManager.getInstance(getActivity()).getUser();
        if (userLogin != null){
            txtFullName.setText(userLogin.getFullName());
            edtFullName.setText(userLogin.getFullName());
            edtPhone.setText(userLogin.getPhone());
            edtAddress.setText(userLogin.getAddress());
            edtJob.setText(userLogin.getJob());
            edtBirthDay.setText(userLogin.getBirthDate());
            if (userLogin.getGender() != null){
                switch (userLogin.getGender()){
                    case "male":
                        rdgGender.check(R.id.rdbMale);
                        break;
                    case "female":
                        rdgGender.check(R.id.rdbFemale);
                        break;
                    default:
                        rdgGender.clearCheck();
                        break;
                }
            }

            if (userLogin.getAvatar() != null){
                byte[] byteArray = Base64.decode(userLogin.getAvatar(), Base64.DEFAULT);
                Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                imgAvatar.setImageBitmap(bmp);
            }
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle(R.string.change_avatar);
        getActivity().getMenuInflater().inflate(R.menu.menu_change_avatar, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.itemChooseImage:
                getImageFromGallery();
                break;
            case R.id.itemTakePhoto:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                        && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA},
                            REQUEST_CAMERA_ACCESS_PERMISSION);
                }else {
                    getImageFromCamera();
                }
                break;
            case R.id.itemViewAvatar:
                Toast.makeText(getActivity(), "View Avatar", Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onContextItemSelected(item);
    }

    private void getImageFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
        }
    }

    private void getImageFromCamera() {
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePicture.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takePicture, TAKE_PICTURE);
        }
    }


    private void addControls(View view) {
        imgAvatar = view.findViewById(R.id.imgAvatar);
        txtFullName = view.findViewById(R.id.txtFullName);
        edtFullName = view.findViewById(R.id.edtFullName);
        edtPhone = view.findViewById(R.id.edtPhone);
        edtBirthDay = view.findViewById(R.id.edtBirthday);
        rdgGender = view.findViewById(R.id.rdgGender);
        edtAddress = view.findViewById(R.id.edtAddress);
        edtJob = view.findViewById(R.id.edtJob);
        btnUpdate = view.findViewById(R.id.btnUpdate);

        pDialog = new ProgressDialog(getContext());
        pDialog.setMessage(getResources().getString(R.string.pd_connecting));
        pDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    InputStream inputStream = getActivity().getContentResolver().openInputStream(data.getData());
                    bitmap = BitmapFactory.decodeStream(inputStream);
                    imgAvatar.setImageBitmap(bitmap);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

            }
        } else if (requestCode == TAKE_PICTURE) {
            if (resultCode == Activity.RESULT_OK) {
                Bundle extras = data.getExtras();
                bitmap = (Bitmap) extras.get("data");
                imgAvatar.setImageBitmap(bitmap);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getImageFromCamera();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}
