package com.example.moneymanager.models;

import java.util.List;

public class DailyTransactionModel {
    String fullDay,totalTrans;
    List<TransactionModel> list;

    public DailyTransactionModel(String fullDay, String totalTrans, List<TransactionModel> list) {
        this.fullDay = fullDay;
        this.totalTrans = totalTrans;
        this.list = list;
    }

    public String getFullDay() {
        return fullDay;
    }

    public void setFullDay(String fullDay) {
        this.fullDay = fullDay;
    }

    public String getTotalTrans() {
        return totalTrans;
    }

    public void setTotalTrans(String totalTrans) {
        this.totalTrans = totalTrans;
    }

    public List<TransactionModel> getList() {
        return list;
    }

    public void setList(List<TransactionModel> list) {
        this.list = list;
    }
}
