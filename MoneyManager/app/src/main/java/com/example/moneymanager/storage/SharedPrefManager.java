package com.example.moneymanager.storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.moneymanager.models.User;

public class SharedPrefManager {

    private static final String SHARED_PREF_NAME = "shared_preff";

    private static SharedPrefManager mInstance;
    private Context mCtx;

    private SharedPrefManager(Context mCtx) {
        this.mCtx = mCtx;
    }


    public static synchronized SharedPrefManager getInstance(Context mCtx) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(mCtx);
        }
        return mInstance;
    }

    public boolean read(String key, boolean defValue){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(key, defValue);
    }

    public void write(String key, boolean value){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putBoolean(key, value);
        editor.apply();
    }

    public void saveUser(User user) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("id", user.get_id());
        editor.putString("email", user.getEmail());
        editor.putString("name", user.getFullName());
        editor.putString("phone", user.getPhone());
        editor.putString("gender", user.getGender());
        editor.putString("address", user.getAddress());
        editor.putString("job", user.getJob());
        editor.putString("avatar", user.getAvatar());
        editor.putString("birthDate", user.getBirthDate());
        editor.putString("token", user.getToken());
        editor.putBoolean("login", true);

        editor.apply();
    }

    public boolean isFirstStart(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("firstStart", true);
    }

    public boolean isLoggedIn() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("login", false);
    }

    public User getUser() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return new User(
                sharedPreferences.getString("id", null),
                sharedPreferences.getString("name", null),
                sharedPreferences.getString("email", null),
                sharedPreferences.getString("phone", null),
                sharedPreferences.getString("gender", null),
                sharedPreferences.getString("address", null),
                sharedPreferences.getString("job", null),
                sharedPreferences.getString("avatar", null),
                sharedPreferences.getString("birthDate", null),
                sharedPreferences.getString("token", null)
        );
    }

    public void clear() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();

        editor.putBoolean("firstStart", false);

        editor.apply();
    }

}
