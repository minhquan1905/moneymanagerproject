package com.example.moneymanager.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.moneymanager.R;
import com.example.moneymanager.activity.DetailTransactionActivity;
import com.example.moneymanager.models.TransactionModel;

import java.util.ArrayList;
import java.util.List;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.HomeViewHolder> {
    private List<TransactionModel> list = new ArrayList<>();
    private Context context;

    public HomeAdapter(Context context){
        this.context = context;
    }

    public void setTransaction(List<TransactionModel> list){
        this.list.addAll(list);
    }

    @NonNull
    @Override
    public HomeViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_home,viewGroup,false);
        return new HomeViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeViewHolder homeViewHolder, int i) {
        TransactionModel model = list.get(i);
        if (model.getIcon() != null){
            byte[] byteArray = Base64.decode(model.getIcon(), Base64.DEFAULT);
            Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            homeViewHolder.icon.setImageBitmap(Bitmap.createScaledBitmap(bmp, 100,
                    100, false));
        }

        homeViewHolder.name.setText(model.getName());
        if(model.getType().trim().equals(context.getString(R.string.chi_tieu))){
            homeViewHolder.price.setText("-" +model.getPrice());
            homeViewHolder.price.setTextColor(context.getResources().getColor(R.color.colorRed));
        } else {
            homeViewHolder.price.setText(model.getPrice());
            homeViewHolder.price.setTextColor(context.getResources().getColor(R.color.colorGreen));
        }

        homeViewHolder.note.setText(model.getNote());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class HomeViewHolder extends RecyclerView.ViewHolder {
        public TextView name,price,note;
        public ImageView icon;

        public HomeViewHolder(@NonNull final View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.icon);
            name = itemView.findViewById(R.id.name);
            price = itemView.findViewById(R.id.price);
            note = itemView.findViewById(R.id.note);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, DetailTransactionActivity.class);
                    Bundle b = new Bundle();
                    b.putString("entryId", list.get(getAdapterPosition()).getId());
                    intent.putExtras(b);
                    context.startActivity(intent);
                }
            });
        }


    }
}
