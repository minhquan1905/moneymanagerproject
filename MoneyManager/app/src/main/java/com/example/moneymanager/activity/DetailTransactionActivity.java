package com.example.moneymanager.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.moneymanager.R;
import com.example.moneymanager.interfaces.GetDataService;
import com.example.moneymanager.models.Entry;
import com.example.moneymanager.storage.SharedPrefManager;
import com.example.moneymanager.utils.RetrofitClientInstance;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailTransactionActivity extends AppCompatActivity {
    private TextView tvCancel, tvTypeTrans, tvGroup, tvNote, tvSave, tvDelete;
    private ImageView imgIcon;
    private EditText edtDate, edtPrice;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    final Calendar myCalendar = Calendar.getInstance();
    private ProgressBar progressBar;
    Bundle b;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_transaction);
        mapId();

        sharedPref = getSharedPreferences("detailTrans", Context.MODE_PRIVATE);
        editor = sharedPref.edit();

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tvTypeTrans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleIntent(DetailTransactionActivity.this,TypeTransactionActivity.class);
            }
        });
        tvGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleIntent(DetailTransactionActivity.this,GroupActivity.class);
            }
        });
        tvNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleIntent(DetailTransactionActivity.this,NoteTransactionActivity.class);
            }
        });
        edtDate.setKeyListener(null);
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };

        edtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(DetailTransactionActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        edtPrice.addTextChangedListener(new NumberTextWatcherForThousand(edtPrice, editor));

        tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLocationDialog();
            }
        });

        b = getIntent().getExtras();
        String entryId;
        if (b != null) {
            entryId = b.getString("entryId", "");

            if (!entryId.isEmpty()){
                setData(entryId);
                editor.putString("entryId", entryId);
            } else {
                getDataBundle();
            }
        } else {
            getDataBundle();
        }

    }

    private void handleIntent(Activity activity1, Class activity2){
        Intent intent = new Intent(activity1, activity2);
        b = new Bundle();
        b.putString("class", "detail");
        intent.putExtras(b);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        finish();
    }

    private void mapId() {
        tvCancel = findViewById(R.id.tvCancel);
        tvTypeTrans = findViewById(R.id.tvTypeTrans);
        tvGroup = findViewById(R.id.tvGroup);
        imgIcon = findViewById(R.id.imgIcon);
        edtDate = findViewById(R.id.edtDate);
        edtPrice = findViewById(R.id.edtPrice);
        tvNote = findViewById(R.id.tvNote);
        tvSave = findViewById(R.id.tvSave);
        progressBar = findViewById(R.id.progressBar);
        tvDelete = findViewById(R.id.tvDelete);
    }

    private void updateLabel() {
        String myFormat = "EEE, dd/M/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        String dateFormat = sdf.format(myCalendar.getTime());
        String day_in_week = dateFormat.substring(0, 3);
        String day_in_month = dateFormat.substring(5, 7);
        String month = dateFormat.substring(8, 9);
        String year = dateFormat.substring(10);

        switch (day_in_week) {
            case "Mon":
                day_in_week = "Thứ Hai";
                break;
            case "Tue":
                day_in_week = "Thứ Ba";
                break;
            case "Wed":
                day_in_week = "Thứ Tư";
                break;
            case "Thu":
                day_in_week = "Thứ Năm";
                break;
            case "Fri":
                day_in_week = "Thứ Sáu";
                break;
            case "Sat":
                day_in_week = "Thứ Bảy";
                break;
            case "Sun":
                day_in_week = "Chủ nhật";
                break;
            default:
                break;
        }

        String convertDate = day_in_week + ", " + day_in_month + " tháng " + month + " " + year;
        edtDate.setText(convertDate);

        editor.putString("edtDate", convertDate);
        editor.apply();
        editor.commit();
    }

    private void setData(String entryId) {
        progressBar.setVisibility(View.VISIBLE);


        final GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<Entry> call = service.getEntryById(entryId);
        call.enqueue(new Callback<Entry>() {
            @Override
            public void onResponse(Call<Entry> call, Response<Entry> response) {
                if (response.isSuccessful()) {
                    progressBar.setVisibility(View.INVISIBLE);
                    tvTypeTrans.setText(response.body().getType());
                    switch (response.body().getType()) {
                        case "Chi tiêu":
                            tvTypeTrans.setTextColor(getResources().getColor(R.color.colorRed));
                            break;
                        case "Thu nhập":
                            tvTypeTrans.setTextColor(getResources().getColor(R.color.colorGreen));
                            break;
                    }
                    editor.putString("tvTypeTrans", response.body().getType());

                    tvGroup.setText(response.body().getName());
                    tvGroup.setTextColor(getResources().getColor(R.color.colorBlack));
                    editor.putString("tvGroup", response.body().getName());

                    edtDate.setText(response.body().getDate());
                    edtDate.setTextColor(getResources().getColor(R.color.colorBlack));
                    editor.putString("edtDate", response.body().getDate());

                    tvNote.setText(response.body().getNote());
                    tvNote.setTextColor(getResources().getColor(R.color.colorBlack));
                    editor.putString("note", response.body().getNote());

                    edtPrice.setText(response.body().getPrice());
                    editor.putString("edtPrice", response.body().getPrice());


                    byte[] byteArray = Base64.decode(response.body().getIcon(), Base64.DEFAULT);
                    Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                    imgIcon.setImageBitmap(Bitmap.createScaledBitmap(bmp, 100,
                            100, false));
                    editor.putString("imgIcon", response.body().getIcon());

                    editor.apply();
                    editor.commit();
//                    getDataBundle();
                }
            }

            @Override
            public void onFailure(Call<Entry> call, Throwable t) {
                Toast.makeText(DetailTransactionActivity.this, "Fail", Toast.LENGTH_LONG).show();
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void getDataBundle() {

        int key = 0;
        String name = "";
        String note = "";
        byte[] byteArray = null;
        if (b != null) {
            key = b.getInt("key");
            name = b.getString("name");
            note = b.getString("note");
            byteArray = b.getByteArray("icon");
        }
        if (key != 0) {
            switch (key) {
                case 1: {
                    tvTypeTrans.setText(R.string.chi_tieu);
                    tvTypeTrans.setTextColor(getResources().getColor(R.color.colorRed));
                    break;
                }
                case 2: {
                    tvTypeTrans.setText(R.string.thu_nhap);
                    tvTypeTrans.setTextColor(getResources().getColor(R.color.colorGreen));
                    break;
                }
            }
            editor.putString("tvTypeTrans", tvTypeTrans.getText().toString());
        }


        if (name != null && !name.isEmpty() && byteArray != null) {
            tvGroup.setText(name);
            tvGroup.setTextColor(getResources().getColor(R.color.colorBlack));
            Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            imgIcon.setImageBitmap(Bitmap.createScaledBitmap(bmp, 100,
                    100, false));

            String encodedImage = Base64.encodeToString(byteArray, Base64.DEFAULT);


            editor.putString("imgIcon", encodedImage);
            editor.putString("tvGroup", tvGroup.getText().toString());


        }
        if (note != null && !note.equals("")) {
            editor.putString("note", note);
        }

        editor.apply();
        editor.commit();

        getDateSharePreferences();
    }

    private void getDateSharePreferences() {
        sharedPref = getSharedPreferences("detailTrans", Context.MODE_PRIVATE);
        final String typeTrans = sharedPref.getString("tvTypeTrans", "");
        final String group = sharedPref.getString("tvGroup", "");
        final String previouslyEncodedImage = sharedPref.getString("imgIcon", "");
        final String date = sharedPref.getString("edtDate", "");
        final String note = sharedPref.getString("note", "");
        final String price = sharedPref.getString("edtPrice", "");
        final String entryId = sharedPref.getString("entryId","");

        if (!typeTrans.isEmpty()) {
            tvTypeTrans.setText(typeTrans);
            switch (typeTrans) {
                case "Chi tiêu":
                    tvTypeTrans.setTextColor(getResources().getColor(R.color.colorRed));
                    break;
                case "Thu nhập":
                    tvTypeTrans.setTextColor(getResources().getColor(R.color.colorGreen));
                    break;
            }

        }
        if (!group.isEmpty()) {
            tvGroup.setText(group);
            tvGroup.setTextColor(getResources().getColor(R.color.colorBlack));
        }
        if (!date.isEmpty()) {
            edtDate.setText(date);
            edtDate.setTextColor(getResources().getColor(R.color.colorBlack));
        }
        if (!note.isEmpty()) {
            tvNote.setText(note);
            tvNote.setTextColor(getResources().getColor(R.color.colorBlack));
        }
        if (!price.isEmpty()) {
            edtPrice.setText(price);
        }
        if (previouslyEncodedImage != null && !previouslyEncodedImage.isEmpty()) {
            byte[] byteArray = Base64.decode(previouslyEncodedImage, Base64.DEFAULT);
            Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            imgIcon.setImageBitmap(Bitmap.createScaledBitmap(bmp, 100,
                    100, false));
        }

        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                String token = "JWT " + SharedPrefManager.getInstance(DetailTransactionActivity.this).getUser().getToken();
//                String userId = SharedPrefManager.getInstance(DetailTransactionActivity.this).getUser().get_id();
                final GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<Entry> call = service.updateOneEntry(token, entryId, price, previouslyEncodedImage, group, typeTrans, date, note);
                call.enqueue(new Callback<Entry>() {
                    @Override
                    public void onResponse(Call<Entry> call, Response<Entry> response) {
                        progressBar.setVisibility(View.INVISIBLE);
//                        editor.putString("entryId", response.body().get_id());
                        Intent intent = new Intent(DetailTransactionActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onFailure(Call<Entry> call, Throwable t) {
                        Toast.makeText(DetailTransactionActivity.this, "Fail", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

    }

    private void showLocationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(DetailTransactionActivity.this);
        builder.setTitle("Bạn muốn xóa giao dịch này?");

        String positiveText = getString(android.R.string.ok);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // positive button logic
                        sharedPref = getSharedPreferences("detailTrans", Context.MODE_PRIVATE);
                        final String entryId = sharedPref.getString("entryId","");
                        String token = "JWT " + SharedPrefManager.getInstance(DetailTransactionActivity.this).getUser().getToken();
                        final GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                        Call<Entry> call = service.deleteOneEntry(token, entryId);
                        call.enqueue(new Callback<Entry>() {
                            @Override
                            public void onResponse(Call<Entry> call, Response<Entry> response) {
                                progressBar.setVisibility(View.INVISIBLE);
//                        editor.putString("entryId", response.body().get_id());
                                Intent intent = new Intent(DetailTransactionActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            }

                            @Override
                            public void onFailure(Call<Entry> call, Throwable t) {
                                Toast.makeText(DetailTransactionActivity.this, "Fail", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                });

        String negativeText = getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // negative button logic
                    }
                });

        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();
    }
}
