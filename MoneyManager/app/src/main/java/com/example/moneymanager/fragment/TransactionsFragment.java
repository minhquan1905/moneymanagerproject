package com.example.moneymanager.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.moneymanager.R;
import com.example.moneymanager.activity.AddTransactionActivity;
import com.example.moneymanager.activity.LoginActivity;
import com.example.moneymanager.activity.MainActivity;
import com.example.moneymanager.adapters.DailyTransAdapter;
import com.example.moneymanager.interfaces.GetDataService;
import com.example.moneymanager.models.DailyTransactionModel;
import com.example.moneymanager.models.Entry;
import com.example.moneymanager.models.TransactionModel;
import com.example.moneymanager.storage.SharedPrefManager;
import com.example.moneymanager.utils.RetrofitClientInstance;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TransactionsFragment extends Fragment {
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    List<DailyTransactionModel> listDailyTransaction = new ArrayList<>();
    private ProgressBar progressBar;

    public static TransactionsFragment newInstance() {

        Bundle args = new Bundle();

        TransactionsFragment fragment = new TransactionsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_transactions, container, false);

        progressBar = view.findViewById(R.id.progressBar);

        sharedPref = getContext().getSharedPreferences("addTrans", Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        if (SharedPrefManager.getInstance(getActivity()).isLoggedIn()) {
            String token = "JWT " + SharedPrefManager.getInstance(getActivity()).getUser().getToken();
            String userId = SharedPrefManager.getInstance(getActivity()).getUser().get_id();

            final GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
            Call<List<Entry>> call = service.getAllEntryById(token, userId);
            progressBar.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<List<Entry>>() {
                @Override
                public void onResponse(Call<List<Entry>> call, Response<List<Entry>> response) {
                    progressBar.setVisibility(View.INVISIBLE);
                    RecyclerView mRecyclerView = view.findViewById(R.id.recyclerView);
                    final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    mRecyclerView.setLayoutManager(layoutManager);
                    DailyTransAdapter adapter = new DailyTransAdapter(getContext());
                    getDataTrans(response.body());
                    adapter.setTransaction(listDailyTransaction);
                    mRecyclerView.setAdapter(adapter);

                }

                @Override
                public void onFailure(Call<List<Entry>> call, Throwable t) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
        }

        return view;
    }

    public void getDataTrans(List<Entry> entryList) {

        List<String> listDate = new ArrayList<>();

        for (Entry entry : entryList) {
            listDate.add(entry.getDate());
        }

        List<String> newListDate = removeDuplicates(listDate);
        for (String date : newListDate) {
            Long totalPrice = 0l;
            List<TransactionModel> list = new ArrayList<>();
            for (Entry entry : entryList) {
                if (entry.getDate().equals(date)) {
                    list.add(new TransactionModel(entry.get_id(), entry.getIcon(), entry.getName(), entry.getPrice(), entry.getNote(), entry.getType()));
                    Long price;
                    if (entry.getType().equals(getActivity().getString(R.string.chi_tieu))) {
                        price = -Long.valueOf(entry.getPrice().replace(",", ""));
                    } else {
                        price = Long.valueOf(entry.getPrice().replace(",", ""));
                    }
                    totalPrice += price;
                }
            }
            listDailyTransaction.add(new DailyTransactionModel(date, NumberFormat.getNumberInstance(Locale.US).format(totalPrice), list));
        }

    }

    public List<String> removeDuplicates(List<String> listDate) {
        List<String> newList = new ArrayList<>();
        for (String date : listDate) {
            if (!newList.contains(date)) {
                newList.add(date);
            }
        }
        return newList;
    }


}
