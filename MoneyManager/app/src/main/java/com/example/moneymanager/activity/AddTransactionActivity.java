package com.example.moneymanager.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.example.moneymanager.R;
import com.example.moneymanager.interfaces.GetDataService;
import com.example.moneymanager.models.Entry;
import com.example.moneymanager.storage.SharedPrefManager;
import com.example.moneymanager.utils.RetrofitClientInstance;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddTransactionActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView tvCancel, tvTypeTrans, tvGroup, tvNote, tvSave;
    private ImageView imgIcon;
    private EditText edtDate, edtPrice;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    final Calendar myCalendar = Calendar.getInstance();
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_transaction);

        mapId();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        sharedPref = getSharedPreferences("editTrans", Context.MODE_PRIVATE);
        editor = sharedPref.edit();

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tvTypeTrans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddTransactionActivity.this, TypeTransactionActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                finish();
            }
        });
        tvGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddTransactionActivity.this, GroupActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                finish();
            }
        });

        tvNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddTransactionActivity.this, NoteTransactionActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                finish();
            }
        });

        edtDate.setKeyListener(null);
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };

        edtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(AddTransactionActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        edtPrice.addTextChangedListener(new NumberTextWatcherForThousand(edtPrice, editor));

        getDataBundle();


    }

    private void mapId() {
        toolbar = findViewById(R.id.toolbar);
        tvCancel = findViewById(R.id.tvCancel);
        tvTypeTrans = findViewById(R.id.tvTypeTrans);
        tvGroup = findViewById(R.id.tvGroup);
        imgIcon = findViewById(R.id.imgIcon);
        edtDate = findViewById(R.id.edtDate);
        edtPrice = findViewById(R.id.edtPrice);
        tvNote = findViewById(R.id.tvNote);
        tvSave = findViewById(R.id.tvSave);
        progressBar = findViewById(R.id.progressBar);
    }

    private void updateLabel() {
        String myFormat = "EEE, dd/M/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        String dateFormat = sdf.format(myCalendar.getTime());
        String day_in_week = dateFormat.substring(0, 3);
        String day_in_month = dateFormat.substring(5, 7);
        String month = dateFormat.substring(8, 9);
        String year = dateFormat.substring(10);

        switch (day_in_week) {
            case "Mon":
                day_in_week = "Thứ Hai";
                break;
            case "Tue":
                day_in_week = "Thứ Ba";
                break;
            case "Wed":
                day_in_week = "Thứ Tư";
                break;
            case "Thu":
                day_in_week = "Thứ Năm";
                break;
            case "Fri":
                day_in_week = "Thứ Sáu";
                break;
            case "Sat":
                day_in_week = "Thứ Bảy";
                break;
            case "Sun":
                day_in_week = "Chủ nhật";
                break;
            default:
                break;
        }

        String convertDate = day_in_week + ", " + day_in_month + " tháng " + month + " " + year;
        edtDate.setText(convertDate);

        editor.putString("edtDate", convertDate);
        editor.apply();
        editor.commit();
    }

    private void getDataBundle() {
        Bundle b = getIntent().getExtras();
        int key = 0;
        String name = "";
        String note = "";
        byte[] byteArray = null;
        if (b != null) {
            key = b.getInt("key");
            name = b.getString("name");
            note = b.getString("note");
            byteArray = b.getByteArray("icon");
        }
        if (key != 0) {
            switch (key) {
                case 1: {
                    tvTypeTrans.setText(R.string.chi_tieu);
                    tvTypeTrans.setTextColor(getResources().getColor(R.color.colorRed));
                    break;
                }
                case 2: {
                    tvTypeTrans.setText(R.string.thu_nhap);
                    tvTypeTrans.setTextColor(getResources().getColor(R.color.colorGreen));
                    break;
                }
            }
            editor.putString("tvTypeTrans", tvTypeTrans.getText().toString());
        }


        if (name != null && !name.isEmpty() && byteArray != null) {
            tvGroup.setText(name);
            tvGroup.setTextColor(getResources().getColor(R.color.colorBlack));
            Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            imgIcon.setImageBitmap(Bitmap.createScaledBitmap(bmp, 100,
                    100, false));

            String encodedImage = Base64.encodeToString(byteArray, Base64.DEFAULT);


            editor.putString("imgIcon", encodedImage);
            editor.putString("tvGroup", tvGroup.getText().toString());


        }
        if (note != null && !note.equals("")) {
            editor.putString("note", note);
        }

        editor.apply();
        editor.commit();

        getDateSharePreferences();
    }

    private void getDateSharePreferences() {
        sharedPref = getSharedPreferences("editTrans", Context.MODE_PRIVATE);
        final String typeTrans = sharedPref.getString("tvTypeTrans", "");
        final String group = sharedPref.getString("tvGroup", "");
        final String previouslyEncodedImage = sharedPref.getString("imgIcon", "");
        final String date = sharedPref.getString("edtDate", "");
        final String note = sharedPref.getString("note", "");
        final String price = sharedPref.getString("edtPrice", "");

        if (!typeTrans.isEmpty()) {
            tvTypeTrans.setText(typeTrans);
            switch (typeTrans) {
                case "Chi tiêu":
                    tvTypeTrans.setTextColor(getResources().getColor(R.color.colorRed));
                    break;
                case "Thu nhập":
                    tvTypeTrans.setTextColor(getResources().getColor(R.color.colorGreen));
                    break;
            }

        }
        if (!group.isEmpty()) {
            tvGroup.setText(group);
            tvGroup.setTextColor(getResources().getColor(R.color.colorBlack));
        }
        if (!date.isEmpty()) {
            edtDate.setText(date);
            edtDate.setTextColor(getResources().getColor(R.color.colorBlack));
        }
        if (!note.isEmpty()) {
            tvNote.setText(note);
            tvNote.setTextColor(getResources().getColor(R.color.colorBlack));
        }
        if (!price.isEmpty()) {
            edtPrice.setText(price);
        }
        if (previouslyEncodedImage != null && !previouslyEncodedImage.isEmpty()) {
            byte[] byteArray = Base64.decode(previouslyEncodedImage, Base64.DEFAULT);
            Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            imgIcon.setImageBitmap(Bitmap.createScaledBitmap(bmp, 100,
                    100, false));
        }

        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                String token ="JWT " + SharedPrefManager.getInstance(AddTransactionActivity.this).getUser().getToken();
                String userId = SharedPrefManager.getInstance(AddTransactionActivity.this).getUser().get_id();
                final GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<Entry> call = service.createEntry(token, price, previouslyEncodedImage, group, userId,typeTrans,date,note);
                call.enqueue(new Callback<Entry>() {
                    @Override
                    public void onResponse(Call<Entry> call, Response<Entry> response) {
                        progressBar.setVisibility(View.INVISIBLE);
                        editor.putString("entryId", response.body().get_id());
                        Intent intent = new Intent(AddTransactionActivity.this,MainActivity.class);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onFailure(Call<Entry> call, Throwable t) {
                        Toast.makeText(AddTransactionActivity.this,"Fail",Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

    }

}
