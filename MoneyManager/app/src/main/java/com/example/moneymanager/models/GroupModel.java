package com.example.moneymanager.models;

import android.graphics.drawable.Drawable;

import com.google.gson.annotations.SerializedName;

public class GroupModel {
    @SerializedName("icon")
    private Drawable icon;
    @SerializedName("name_group")
    private String name;


    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GroupModel(Drawable icon, String name) {
        this.icon = icon;
        this.name = name;
    }
}
