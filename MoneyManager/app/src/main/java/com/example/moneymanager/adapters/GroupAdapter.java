package com.example.moneymanager.adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.example.moneymanager.R;
import com.example.moneymanager.activity.AddTransactionActivity;
import com.example.moneymanager.activity.DetailTransactionActivity;
import com.example.moneymanager.activity.GroupActivity;
import com.example.moneymanager.activity.NoteTransactionActivity;
import com.example.moneymanager.models.GroupModel;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class GroupAdapter extends RecyclerView.Adapter<GroupAdapter.GroupViewHolder> {
    private List<GroupModel> list = new ArrayList<>();
    private Activity activity;

    public GroupAdapter(Activity activity, List<GroupModel> list){
        this.activity = activity;
        this.list.addAll(list);
    }

    @NonNull
    @Override
    public GroupViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_group_item,viewGroup,false);

        return new GroupViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull GroupViewHolder groupViewHolder, int i) {
        GroupModel model = list.get(i);
        groupViewHolder.icon.setImageDrawable(model.getIcon());
        groupViewHolder.name.setText(model.getName());
    }

    @Override
    public int getItemCount() {
        System.out.println("size" + list.size());
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class GroupViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public ImageView icon;

        public GroupViewHolder(@NonNull View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.imgIcon);
            name = itemView.findViewById(R.id.tvName);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(v.getContext(), "Clicked!!!", Toast.LENGTH_SHORT).show();
//                    Intent intent = new Intent(v.getContext(), AddTransactionActivity.class);

                    Drawable d = icon.getDrawable();
                    Bitmap bitmap = ((BitmapDrawable) d).getBitmap();
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] byteArray = stream.toByteArray();

                    Intent intent;
                    Bundle b1;
                    b1 = activity.getIntent().getExtras();
                    if (b1 != null) {
                        if (b1.getString("class").equals("detail")) {
                            intent = new Intent(activity, DetailTransactionActivity.class);
                        } else {
                            intent = new Intent(activity, AddTransactionActivity.class);
                        }
                    } else {
                        intent = new Intent(activity, AddTransactionActivity.class);
                    }
                    Bundle b2 = new Bundle();


                    b2.putString("name", name.getText().toString());
                    b2.putByteArray("icon", byteArray);

                    intent.putExtras(b2);
                    activity.startActivity(intent);
                    activity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                    activity.finish();
                }
            });
        }

    }


}
