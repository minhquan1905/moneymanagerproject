package com.example.moneymanager.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.example.moneymanager.R;

public class NoteTransactionActivity extends AppCompatActivity {
    private TextView tvCancel, tvSave;
    private EditText edtNote;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_transaction);

        mapId();

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleChange("");
            }
        });

        edtNote.requestFocus();
        showKeyBoard();

        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleChange(edtNote.getText().toString());
            }
        });

    }

    private void showKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    private void hideKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edtNote.getWindowToken(), 0);
    }

    private void mapId() {
        tvCancel = findViewById(R.id.tvCancel);
        edtNote = findViewById(R.id.edtNote);
        tvSave = findViewById(R.id.tvSave);
    }

    private void handleChange(String note) {
        hideKeyBoard();
        Intent intent;
        Bundle b1;
        b1 = getIntent().getExtras();
        if (b1 != null) {
            if (b1.getString("class").equals("detail")) {
                intent = new Intent(NoteTransactionActivity.this, DetailTransactionActivity.class);
            } else {
                intent = new Intent(NoteTransactionActivity.this, AddTransactionActivity.class);
            }
        } else {
            intent = new Intent(NoteTransactionActivity.this, AddTransactionActivity.class);
        }
        Bundle b2 = new Bundle();
        b2.putString("note", note);
        intent.putExtras(b2);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }

}
