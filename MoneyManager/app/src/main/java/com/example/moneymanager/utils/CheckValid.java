package com.example.moneymanager.utils;

import android.util.Patterns;
import android.widget.EditText;

public class CheckValid {
    public static boolean checkEditText(EditText editText) {
        if (editText.getText().toString().trim().length() > 0)
            return true;
        else {
            editText.setError("Vui lòng nhập dữ liệu!");
            editText.requestFocus();
        }
        return false;
    }

    public static boolean checkLengthPassword(EditText editText) {
        if (editText.getText().toString().trim().length() < 6 ){
            editText.setError("Mật khẩu quá ngắn. Độ dài tối thiểu là 6 ký tự.");
            editText.requestFocus();
            return false;
        }
        return true;
    }

    public static boolean checkPassword(EditText edtNewPassword, EditText edtConfirmPassword){
        if (edtNewPassword.getText().toString().trim().equals(edtConfirmPassword.getText().toString().trim())){
            return true;
        } else {
            edtConfirmPassword.setError("Mật khẩu phải giống nhau");
        }
        return false;
    }

    public static boolean isValidEmail(EditText edtEmail) {
        String email = edtEmail.getText().toString();
        if (Patterns.EMAIL_ADDRESS.matcher(email).matches())
            return true;
        else {
            edtEmail.setError("Email sai định dạng!");
            edtEmail.requestFocus();
        }
        return false;
    }
}
