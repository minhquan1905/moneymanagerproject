package com.example.moneymanager.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.moneymanager.R;
import com.example.moneymanager.interfaces.GetDataService;
import com.example.moneymanager.models.User;
import com.example.moneymanager.storage.SharedPrefManager;
import com.example.moneymanager.utils.CheckValid;
import com.example.moneymanager.utils.RetrofitClientInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChagePasswordActivity extends BaseActivity {

    private Toolbar toolbar;
    private EditText edtOldPassword, edtNewPassword, edtConfirmPassword;
    private Button btnUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chage_password);
        addControls();
        addEvents();
    }

    private void addEvents() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               String oldPassword = edtOldPassword.getText().toString().trim();
               String newPassword = edtNewPassword.getText().toString().trim();

               if (CheckValid.checkEditText(edtOldPassword) || CheckValid.checkEditText(edtNewPassword) ||
                       CheckValid.checkEditText(edtConfirmPassword)){

                   if (CheckValid.checkPassword(edtNewPassword, edtConfirmPassword)){
                       updatePassword(oldPassword, newPassword);
                   }
               }
            }
        });
    }

    private void updatePassword(String oldPassword, String newPassword) {
        User user = SharedPrefManager.getInstance(ChagePasswordActivity.this).getUser();
        final GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<User> call = service.changePassword("JWT " + user.getToken(), user.get_id() ,newPassword);
        
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()){
                    Toast.makeText(ChagePasswordActivity.this, "Đổi mật khẩu thành công", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(ChagePasswordActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void addControls() {
        edtOldPassword = findViewById(R.id.edtOldPassword);
        edtNewPassword = findViewById(R.id.edtNewPassword);
        edtConfirmPassword = findViewById(R.id.edtConfirmPassword);
        btnUpdate = findViewById(R.id.btnUpdate);

        toolbar = findViewById(R.id.toolbarChangePassword);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }
}
