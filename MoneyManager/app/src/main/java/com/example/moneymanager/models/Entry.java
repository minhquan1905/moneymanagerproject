package com.example.moneymanager.models;

import com.google.gson.annotations.SerializedName;

public class Entry {
    @SerializedName("_id")
    private String _id;

    @SerializedName("price")
    private String price;

    @SerializedName("icon")
    private String icon;

    @SerializedName("name")
    private String name;

    @SerializedName("userId")
    private String userId;

    @SerializedName(("type"))
    private String type;

    @SerializedName(("date"))
    private String date;

    @SerializedName(("note"))
    private String note;

    public Entry(String _id, String price, String icon, String name, String userId, String type, String date, String note) {
        this._id = _id;
        this.price = price;
        this.icon = icon;
        this.name = name;
        this.userId = userId;
        this.type = type;
        this.date = date;
        this.note = note;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
