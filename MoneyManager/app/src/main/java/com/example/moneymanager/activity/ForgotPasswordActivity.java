package com.example.moneymanager.activity;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.moneymanager.R;

import static com.example.moneymanager.utils.CheckValid.isValidEmail;

public class ForgotPasswordActivity extends BaseActivity {

    Toolbar toolbar;
    EditText edtEmail;
    Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        edtEmail = findViewById(R.id.edtEmail);
        btnSubmit = findViewById(R.id.btnSubmit);
        toolbar = findViewById(R.id.toolbar_forgot_password);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidEmail(edtEmail)){
                    new AlertDialog.Builder(ForgotPasswordActivity.this)
                            .setTitle("Hừm...")
                            .setMessage("Vui lòng kiểm tra email của bạn")
                            .setPositiveButton(android.R.string.ok, null)
                            .show();
                }
            }
        });


    }
}
