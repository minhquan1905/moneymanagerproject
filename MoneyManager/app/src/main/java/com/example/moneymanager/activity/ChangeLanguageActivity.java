package com.example.moneymanager.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.moneymanager.R;
import com.example.moneymanager.utils.LocalManageUtil;
import com.example.moneymanager.utils.SPUtil;

public class ChangeLanguageActivity extends BaseActivity {

    private Toolbar toolbar;
    private RadioGroup rdgLanguage;
    private RadioButton rdbVN, rdbEN;

    private final int VIETNAM = 0;
    private final int ENGLISH= 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_language);
        addControls();
        addEvents();
    }

    private void addEvents() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        switch (SPUtil.getInstance(getApplicationContext()).getSelectLanguage()) {
            case VIETNAM:
                rdbVN.setChecked(true);
                break;
            case ENGLISH:
                rdbEN.setChecked(true);
                break;
        }

        rdgLanguage.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.rdbVN:
                        selectLanguage(VIETNAM);
                        break;
                    case R.id.rdbEN:
                        selectLanguage(ENGLISH);
                        break;
                }
            }
        });
    }

    private void addControls() {
        rdgLanguage = findViewById(R.id.rdgLanguage);
        rdbVN = findViewById(R.id.rdbVN);
        rdbEN = findViewById(R.id.rdbEN);
        toolbar = findViewById(R.id.toolbarLanguage);
    }

    private void selectLanguage(int select) {
        LocalManageUtil.saveSelectLanguage(this, select);
        MainActivity.reStart(this);
    }

}
