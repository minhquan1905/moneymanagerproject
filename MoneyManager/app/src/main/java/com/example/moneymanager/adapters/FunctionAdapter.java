package com.example.moneymanager.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.moneymanager.R;
import com.example.moneymanager.models.Function;

import java.util.List;

public class FunctionAdapter extends BaseAdapter {

    private List<Function> functionList;
    private LayoutInflater layoutInflater;
    private Context context;

    public FunctionAdapter(Context context, List<Function> functionList) {
        this.context = context;
        this.functionList = functionList;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return functionList.size();
    }

    @Override
    public Object getItem(int position) {
        return functionList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_item_layout, null);
            holder = new ViewHolder();
            holder.imgIcon = convertView.findViewById(R.id.imgIcon);
            holder.txtFunctionName = convertView.findViewById(R.id.txtFunctionName);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Function function = functionList.get(position);

        holder.txtFunctionName.setText(function.getName());

        int imageId =getMipmapResIdByName(function.getImageName());
        holder.imgIcon.setImageResource(imageId);

        return convertView;
    }

    public int getMipmapResIdByName(String resName)  {
        String pkgName = context.getPackageName();

        int resID = context.getResources().getIdentifier(resName , "drawable", pkgName);
        return resID;
    }

    static class ViewHolder {
        ImageView imgIcon;
        TextView txtFunctionName;
    }
}
