package com.example.moneymanager.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.moneymanager.R;
import com.example.moneymanager.interfaces.GetDataService;
import com.example.moneymanager.models.User;
import com.example.moneymanager.utils.RetrofitClientInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.moneymanager.utils.CheckValid.checkEditText;
import static com.example.moneymanager.utils.CheckValid.checkLengthPassword;
import static com.example.moneymanager.utils.CheckValid.isValidEmail;

public class RegisterActivity extends BaseActivity {

    private EditText edtFullName, edtEmail, edtPassword;
    private Button btnRegister;
    private Toolbar toolbar;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        addControls();
        addEvents();
    }

    private void addEvents() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fullName = edtFullName.getText().toString().trim();
                String email = edtEmail.getText().toString().trim();
                String password = edtPassword.getText().toString().trim();

                registerUser(email, password, fullName);
            }
        });
    }

    private void registerUser(String email, String password, String fullName) {
        if (checkEditText(edtEmail) && checkEditText(edtFullName) && isValidEmail(edtEmail)
        && checkLengthPassword(edtPassword)) {
            pDialog.show();
            final GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
            Call<User> call = service.register(email, password, fullName);
            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    pDialog.dismiss();
                    if (response.isSuccessful()){
                        startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
                    } else {
                        new AlertDialog.Builder(RegisterActivity.this)
                                .setTitle("Hừm...")
                                .setMessage(getResources().getString(R.string.error_username))
                                .setPositiveButton(android.R.string.ok, null)
                                .show();
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    pDialog.dismiss();
                    new AlertDialog.Builder(RegisterActivity.this)
                            .setTitle("Hừm...")
                            .setMessage(getResources().getString(R.string.error_connec))
                            .setPositiveButton(android.R.string.ok, null)
                            .show();
                }
            });
        }
    }

    private void addControls() {
        edtFullName = findViewById(R.id.edtFullName);
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);
        btnRegister = findViewById(R.id.btnRegister);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage(getResources().getString(R.string.pd_connecting));
        pDialog.setCanceledOnTouchOutside(false);

        toolbar = findViewById(R.id.toolbarRegister);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }
}
