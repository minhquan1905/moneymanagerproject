package com.example.moneymanager.models;

public class TransactionModel {
    String id, name,price,note,type;
    String icon;

    public TransactionModel(String id, String icon, String name, String price, String note, String type) {
        this.id = id;
        this.icon = icon;
        this.name = name;
        this.price = price;
        this.note = note;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
