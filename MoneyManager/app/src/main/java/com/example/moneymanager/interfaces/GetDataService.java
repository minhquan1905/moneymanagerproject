package com.example.moneymanager.interfaces;

import com.example.moneymanager.models.Entry;
import com.example.moneymanager.models.User;

import java.util.List;

import java.util.Date;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GetDataService {
    @FormUrlEncoded
    @POST("/auth/sign_in")
    Call<User> signIn(@Field("email") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST("auth/register")
    Call<User> register(@Field("email") String email, @Field("password") String password,
                        @Field("fullName") String fullName);

    @FormUrlEncoded
    @POST("/entry")
    Call<Entry> createEntry(@Header("authorization") String token, @Field("price") String price, @Field("icon") String icon,
                            @Field("name") String name, @Field("userId") String userId, @Field("type") String type,
                            @Field("date") String date, @Field("note") String note);

    @FormUrlEncoded
    @PUT("/update-profile/{id}")
    Call<User> updateProfile(@Header("authorization") String token, @Path("id") String id, @Field("fullName") String fullName,
                             @Field("phone") String phone, @Field("gender") String gender,
                             @Field("address") String address, @Field("job") String job, @Field("birthDate") String birthDate,
                             @Field("avatar") String avatar);
    @FormUrlEncoded
    @PUT("/update-profile/{id}")
    Call<User> changePassword(@Header("authorization") String token, @Path("id") String id, @Field("password") String password);


    @GET("/entry/{id}")
    Call<Entry> getEntryById(@Path("id") String id);

    @GET("/entries")
    Call<List<Entry>> getAllEntryById(@Header("authorization") String token, @Query("userid") String id);

    @FormUrlEncoded
    @PUT("/entry/{id}")
    Call<Entry> updateOneEntry(@Header("authorization") String token, @Path("id") String id, @Field("price") String price, @Field("icon") String icon,
                               @Field("name") String name, @Field("type") String type,
                               @Field("date") String date, @Field("note") String note);

    @DELETE("/entry/{id}")
    Call<Entry> deleteOneEntry(@Header("authorization") String token, @Path("id") String id);
}
